//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		    /*Update Interval*/	/*Update Signal*/
	{"", "systray",				    1,			        0},
	{"", "volstat",				    0,			        10},
	{"", "micstat",				    0,			        11},
	{"", "netstat",				    2,			        0},
	{"", "batstat",				    1,			        0},
	{"󰌌 ", "layoutstat",			0,			        12},
/*	{" ", "memorystat",			1,			        0},*/
/*	{" ", "cpustat",				1,			        0},*/
/*	{"󰯎 ", "netstat",  			    2,			        0},*/
	{" ", "date '+%I:%M%p'",	    1,			        0},
	{" ", "date '+%a %Y-%m-%d'",	1,			        0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "    ";
static unsigned int delimLen = 15;
